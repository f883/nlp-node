const { NlpManager } = require('node-nlp');
const http = require('http')
const port = 3000
var url = require('url');

function addUserPhrase(fileName, text, index) {
    manager = new NlpManager();
    manager.load(fileName);
    manager.addDocument('ru', text, index);
    manager.save(fileName);
}

function addAnswer(fileName, index, text) {
    manager = new NlpManager();
    manager.load(fileName);
    manager.addAnswer('ru', index, text);
    manager.save(fileName);
}

// имя используется для идентификации компании
async function createNewFile(fileName) {
    const manager = new NlpManager({ languages: ['ru'] });
    await manager.train();
    manager.save(fileName);
}

async function getAnswer(fileName, text) {
    manager = new NlpManager();
    manager.load(fileName);
    return await manager.process('ru', text);
}

const requestHandler = (request, response) => {
    var url_parts = url.parse(request.url, true);
    var query = url_parts.query;
    console.log('request: ' + query['text']);
    getAnswer('model_2.nlp', query['text']).then(resp =>
        response.end(JSON.stringify(resp))
    );
}


async function main() {
    let fl = 'model_2.nlp';
    await createNewFile(fl);
    addUserPhrase(fl, 'привет', 'index_1');
    addUserPhrase(fl, 'дороу', 'index_2');

    addAnswer(fl, 'index_1', 'hi');
    addAnswer(fl, 'index_2', 'hello');

    http.createServer(requestHandler).listen(port, (err) => {
        if (err) {
            return console.log('something bad happened', err)
        }
        console.log(`server is listening on ${port}`)
    })
}

main();